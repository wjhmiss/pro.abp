﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Volo.Abp.Application.Dtos;
using Volo.Abp.Application.Services;
using Volo.Abp.AspNetCore.Mvc;
using Volo.Abp.LanguageManagement.Dto;

namespace Volo.Abp.LanguageManagement
{
    [Area("languageManagement")]
	[RemoteService(true, Name = LanguageManagementRemoteServiceConsts.RemoteServiceName)]
	[Route("api/language-management/languages")]
	[ControllerName("Languages")]
	public class LanguageController : AbpController, ILanguageAppService, IRemoteService, IApplicationService
	{
		protected ILanguageAppService LanguageAppService { get; }

		public LanguageController(ILanguageAppService languageAppService)
		{
			this.LanguageAppService = languageAppService;
		}

		[Route("all")]
		[HttpGet]
		public virtual async Task<ListResultDto<LanguageDto>> GetAllListAsync()
		{
			return await this.LanguageAppService.GetAllListAsync();
		}

		[HttpGet]
		public virtual async Task<ListResultDto<LanguageDto>> GetListAsync(GetLanguagesTextsInput input)
		{
			return await this.LanguageAppService.GetListAsync(input);
		}

		[Route("{id}")]
		[HttpGet]
		public virtual async Task<LanguageDto> GetAsync(Guid id)
		{
			return await this.LanguageAppService.GetAsync(id);
		}

		[HttpPost]
		public virtual async Task<LanguageDto> CreateAsync(CreateLanguageDto input)
		{
			return await this.LanguageAppService.CreateAsync(input);
		}

		[HttpPut]
		[Route("{id}")]
		public virtual async Task<LanguageDto> UpdateAsync(Guid id, UpdateLanguageDto input)
		{
			return await this.LanguageAppService.UpdateAsync(id, input);
		}

		[HttpDelete]
		[Route("{id}")]
		public virtual async Task DeleteAsync(Guid id)
		{
			await this.LanguageAppService.DeleteAsync(id);
		}

		[Route("{id}/set-as-default")]
		[HttpPut]
		public virtual async Task SetAsDefaultAsync(Guid id)
		{
			await this.LanguageAppService.SetAsDefaultAsync(id);
		}

		[Route("resources")]
		[HttpGet]
		public virtual Task<List<LanguageResourceDto>> GetResourcesAsync()
		{
			return this.LanguageAppService.GetResourcesAsync();
		}

		[HttpGet]
		[Route("culture-list")]
		public virtual Task<List<CultureInfoDto>> GetCulturelistAsync()
		{
			return this.LanguageAppService.GetCulturelistAsync();
		}
	}
}
