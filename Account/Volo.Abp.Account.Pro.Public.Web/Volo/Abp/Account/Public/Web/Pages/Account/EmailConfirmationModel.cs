﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace Volo.Abp.Account.Public.Web.Pages.Account
{
	public class EmailConfirmationModel : AccountPageModel
	{
		public EmailConfirmationModel(IAccountAppService accountAppService)
		{
			this.AccountAppService = accountAppService;
		}

		[BindProperty(SupportsGet = true)]
		public Guid? TenantId { get; set; }

		[Required]
		[BindProperty(SupportsGet = true)]
		public Guid UserId { get; set; }

		[BindProperty(SupportsGet = true)]
		[Required]
		public string ConfirmationToken { get; set; }

		public virtual async Task OnGetAsync()
		{
			this.CheckCurrentTenant(this.TenantId);
			var identityUser = await base.UserManager.GetByIdAsync(this.UserId);
			if (!identityUser.EmailConfirmed)
			{
				var identityResult = await base.UserManager.ConfirmEmailAsync(identityUser, this.ConfirmationToken);
			}
		}

		public virtual Task<IActionResult> OnPostAsync()
		{
			return Task.FromResult<IActionResult>(this.Page());
		}
	}
}
