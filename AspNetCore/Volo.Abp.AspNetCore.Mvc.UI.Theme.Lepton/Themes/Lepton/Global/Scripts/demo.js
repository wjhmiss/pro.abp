﻿const inputs = document.querySelectorAll('.color-box > input')
const root = document.documentElement

inputs.forEach(input => {
    input.addEventListener('input', handleInputChange)
})

function handleInputChange(e) {
    let value = e.target.value
    let inputId = e.target.parentNode.id
    let inputBg = `--${inputId}`
    root.style.setProperty(inputBg, value)
}

function handleSlider(e) {
    let value = e.target.value
    root.style.setProperty('--slider', value)
}

//////////////

$(document).ready(function () {
    var cssLepton1 = "<link rel=\"stylesheet\" href=\"/Themes/Lepton/Global/Styles/lepton1.css\">"; 
    var cssLepton2 = "<link rel=\"stylesheet\" href=\"/Themes/Lepton/Global/Styles/lepton2.css\">";
    var cssLepton3 = "<link rel=\"stylesheet\" href=\"/Themes/Lepton/Global/Styles/lepton3.css\">";
    var cssLepton4 = "<link rel=\"stylesheet\" href=\"/Themes/Lepton/Global/Styles/lepton4.css\">";
    var cssLepton5 = "<link rel=\"stylesheet\" href=\"/Themes/Lepton/Global/Styles/lepton5.css\">";
    var cssLepton6 = "<link rel=\"stylesheet\" href=\"/Themes/Lepton/Global/Styles/lepton6.css\">";

    $(".demo-style-1").on("click", function () {
        $("body").append(cssLepton1);
        $('#lepton-demo-styles').find(".selected-style").removeClass("selected-style");
        $(this).addClass("selected-style");
        $("body").removeClass(function (index, css) {
            return (css.match(/\blepton-\S+/g) || []).join(' '); // removes anything that starts with "lepton-"
        });
        $("body").addClass("lepton-1"); 
    });
    $(".demo-style-2").on("click", function () {
        $("body").append(cssLepton2);
        $('#lepton-demo-styles').find(".selected-style").removeClass("selected-style");
        $(this).addClass("selected-style");
        $("body").removeClass(function (index, css) {
            return (css.match(/\blepton-\S+/g) || []).join(' '); // removes anything that starts with "lepton-"
        });
        $("body").addClass("lepton-2"); 
    });
    $(".demo-style-3").on("click", function () {
        $("body").append(cssLepton3);
        $('#lepton-demo-styles').find(".selected-style").removeClass("selected-style");
        $(this).addClass("selected-style");
        $("body").removeClass(function (index, css) {
            return (css.match(/\blepton-\S+/g) || []).join(' '); // removes anything that starts with "lepton-"
        });
        $("body").addClass("lepton-3"); 
    });
    $(".demo-style-4").on("click", function () {
        $("body").append(cssLepton4);
        $('#lepton-demo-styles').find(".selected-style").removeClass("selected-style");
        $(this).addClass("selected-style");
        $("body").removeClass(function (index, css) {
            return (css.match(/\blepton-\S+/g) || []).join(' '); // removes anything that starts with "lepton-"
        });
        $("body").addClass("lepton-4"); 
    });
    $(".demo-style-5").on("click", function () {
        $("body").append(cssLepton5);
        $('#lepton-demo-styles').find(".selected-style").removeClass("selected-style");
        $(this).addClass("selected-style");
        $("body").removeClass(function (index, css) {
            return (css.match(/\blepton-\S+/g) || []).join(' '); // removes anything that starts with "lepton-"
        });
        $("body").addClass("lepton-5"); 
    });
    $(".demo-style-6").on("click", function () {
        $("body").append(cssLepton6);
        $('#lepton-demo-styles').find(".selected-style").removeClass("selected-style");
        $(this).addClass("selected-style");
        $("body").removeClass(function (index, css) {
            return (css.match(/\blepton-\S+/g) || []).join(' '); // removes anything that starts with "lepton-"
        });
        $("body").addClass("lepton-6"); 
    });
    $(".demo-toggler").on("click", function () {
        $('.demo-container').toggleClass("demo-open");
    });

    if ($('#LeptonThemeSettingsForm').length > 0) {
        var applyLayoutSettingCookie = function (setStyle) {
            var cookie = abp.utils.getCookieValue("LeptonThemeDemoPreferences");

            if (!cookie || cookie === '') {
                cookie = 'menuPlacement:left|MenuStatus:0|boxed:false`|style:demo-style-1';
            }

            var cookieValues = cookie.split("|");
            var menuPlacement = '';
            var menuStatus = '';
            var boxed = '';
            var style = '';

            for (var i = 0; i < cookieValues.length; i++) {
                var keyValueArr = cookieValues[i].split(":");
                var key = keyValueArr[0];
                var value = keyValueArr[1];

                if (key === 'menuPlacement') {
                    menuPlacement = value;
                }
                else if (key === 'menuStatus') {
                    menuStatus = value;
                }
                else if (key === 'boxed') {
                    boxed = value;
                }
                else if (key === 'style') {
                    style = value;
                }
            }

            $('body').removeClass('lp-boxed');
            $('body').removeClass('lp-topmenu');
            $('body').removeClass('lp-opened-sidebar');
            $('body').removeClass('lp-closed');

            if (boxed.toLowerCase() === 'true') {
                $('body').addClass('lp-boxed');
                $('#BoxedLayout').prop('checked', true);
            }
            else {
                $('#BoxedLayout').prop('checked', false);
            }

            if (menuPlacement == 1) {
                $('body').addClass('lp-topmenu');
                $("#MenuPlacement").attr('selectedIndex', 1);
                $("#MenuPlacement").val(1);
            }
            else {
                $("#MenuPlacement").attr('selectedIndex', 0);
                $("#MenuPlacement").val(0);

                if (menuStatus == 0) {
                    $('body').addClass('lp-opened-sidebar');

                    $("#MenuStatus").attr('selectedIndex', 0);
                    $("#MenuStatus").val(0);
                }
                else if (menuStatus == 1) {
                    $('body').addClass('lp-closed');

                    $("#MenuStatus").attr('selectedIndex', 1);
                    $("#MenuStatus").val(1);
                }
            }

            if (setStyle) {
                $('.' + style).click();
            }
        };

        var setLayoutSettingCookie = function (e) {
            if (e) {
                e.preventDefault();
            }

            var getOneYearLater = function () {
                var tenYearsLater = new Date();
                tenYearsLater.setTime(tenYearsLater.getTime() + (365 * 24 * 60 * 60 * 1000));
                return tenYearsLater;
            };

            var form = $('#LeptonThemeSettingsForm').serializeFormToObject();

            var formAsString =
                'menuPlacement:' +
                form.menuPlacement +
                '|' +
                'menuStatus:' +
                form.menuStatus +
                '|' +
                'boxed:' +
                form.boxedLayout +
                '|' +
                'style:' +
                $('.selected-style').attr('style-id');

            abp.utils.setCookieValue('LeptonThemeDemoPreferences', formAsString, getOneYearLater(), '/');

            applyLayoutSettingCookie();
        };

        $('#LeptonThemeSettingsForm input').on('change', '', function (e) {
            setLayoutSettingCookie(e);
        });

        $('#LeptonThemeSettingsForm select').on('change', '', function (e) {
            setLayoutSettingCookie(e);
        });

        $(".demo-style").on("click", function () {
            setLayoutSettingCookie();
        });

        applyLayoutSettingCookie(true);
    }
});
