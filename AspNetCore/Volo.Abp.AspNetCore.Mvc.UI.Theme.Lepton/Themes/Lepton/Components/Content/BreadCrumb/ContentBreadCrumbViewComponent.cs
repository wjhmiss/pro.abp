﻿using Microsoft.AspNetCore.Mvc;
using Volo.Abp.AspNetCore.Mvc.UI.Layout;

namespace Volo.Abp.AspNetCore.Mvc.UI.Theme.Lepton.Themes.Lepton.Components.Content.BreadCrumb
{
    public class ContentBreadCrumbViewComponent : LeptonViewComponentBase
	{
		protected IPageLayout PageLayout { get; }

		public ContentBreadCrumbViewComponent(IPageLayout pageLayout)
		{
			this.PageLayout = pageLayout;
		}

		public IViewComponentResult Invoke()
		{
			return base.View<ContentLayout>("~/Themes/Lepton/Components/Content/BreadCrumb/Default.cshtml", this.PageLayout.Content);
		}
	}
}
