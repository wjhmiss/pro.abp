﻿using Microsoft.AspNetCore.Mvc;
using Volo.Abp.AspNetCore.Mvc.UI.Alerts;

namespace Volo.Abp.AspNetCore.Mvc.UI.Theme.Lepton.Themes.Lepton.Components.Content.Alerts
{
    public class ContentAlertsViewComponent : LeptonViewComponentBase
	{
		private readonly IAlertManager _alertManager;

		public ContentAlertsViewComponent(IAlertManager alertManager)
		{
			this._alertManager = alertManager;
		}

		public IViewComponentResult Invoke(string name)
		{
			return base.View<AlertList>("~/Themes/Lepton/Components/Content/Alerts/Default.cshtml", this._alertManager.Alerts);
		}
	}
}
