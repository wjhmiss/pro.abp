﻿namespace Volo.Abp.LeptonTheme.Management
{
    public static class LeptonThemeSettingNames
	{
		public const string Style = "Volo.Abp.LeptonTheme.Style";

		public static class Layout
		{
			public const string Boxed = "Volo.Abp.LeptonTheme.Layout.Boxed";

			public const string MenuPlacement = "Volo.Abp.LeptonTheme.Layout.MenuPlacement";

			public const string MenuStatus = "Volo.Abp.LeptonTheme.Layout.MenuStatus";
		}
	}
}
