﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Localization;
using System.Threading.Tasks;
using Volo.Abp.LeptonTheme.Management.Localization;
using Volo.Abp.LeptonTheme.Management.Pages.LeptonThemeManagement.Components.LeptonThemeSettingGroup;
using Volo.Abp.SettingManagement.Web.Pages.SettingManagement;

namespace Volo.Abp.LeptonTheme.Management.Settings
{
    public class LeptonThemeSettingManagementPageContributor : ISettingPageContributor
	{
		public async Task ConfigureAsync(SettingPageCreationContext context)
		{
			var flag = await CheckPermissionsAsync(context);
			if (flag)
			{
				var requiredService = context.ServiceProvider.GetRequiredService<IStringLocalizer<LeptonThemeManagementResource>>();
				context.Groups.Add(new SettingPageGroup("Volo.Abp.LeptonThemeManagement", requiredService["Menu:LeptonThemeSettings"], typeof(LeptonThemeSettingGroupViewComponent), null));
			}
		}

		public async Task<bool> CheckPermissionsAsync(SettingPageCreationContext context)
		{
			var requiredService = context.ServiceProvider.GetRequiredService<IAuthorizationService>();
			return await requiredService.IsGrantedAsync("LeptonThemeManagement.Settings");
		}
	}
}
